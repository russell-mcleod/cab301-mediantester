#MedianTester

Tests the Time Efficiency of Median() and BruteForceMedian().  
Does this by counting Basic Operations and Execution time (in CPU ticks).

Should basically be complete now.  
Tests arrays with these 2 algorithms with sizes from 100 to 5000, incrementing the size by 100 after each test, for a total of 50 tests.

