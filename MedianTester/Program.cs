﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedianTester
{
    static class Program
    {
        /// <summary>
        /// Global variable to store number of basic operations counted during algorithm execution.
        /// </summary>
        public static int basicOperations = 0;

        /// <summary>
        /// Main method of test program. First runs Functionality Test for BruteForceMedian and Median,
        /// and if that passes, runs all other tests with integer arrays with sizes starting at 100,
        /// incrementing the size by 100, until the size is higher than 5000, resulting in each test being run 50 times.
        /// </summary>
        /// <param name="args">command line args for Main. Not used by program.</param>
        static void Main(string[] args)
        {
            Console.WriteLine("MedianTester loaded!" + Environment.NewLine);
            Console.WriteLine("WARNING:" + Environment.NewLine + "This program calls recursive algorithms with large data sizes," + Environment.NewLine + "and thus requires the computer running it to have a lot of RAM installed. (8GB+ recommended)");
            Console.WriteLine("If this programs fails with a StackOverflow error, try running it on a computer with more RAM.");

            Console.WriteLine(Environment.NewLine + "Press any key to begin..." + Environment.NewLine);
            Console.ReadKey();

            Console.WriteLine("Testing Functional Correctness...");

            if (!FunctionalityTest())
            {
                Console.WriteLine("FUNCTIONAL TEST FAIL, Aborting tests...");
            }
            else
            {
                Console.WriteLine("FUNCTIONAL TEST SUCCESSFUL, Continuing with tests...");
                Console.WriteLine();

                Console.WriteLine("STOPWATCH FREQUENCY (CPU Ticks per Second): " + Stopwatch.Frequency.ToString());
                Console.WriteLine();

                if (!Directory.Exists("test_results"))
                {
                    try
                    {
                        Directory.CreateDirectory("test_results");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Cannot create test_results dir");
                        Console.WriteLine("Error Message: " + ex.Message);
                        Console.WriteLine("Aborting tests...");
                        return;
                    }
                }
                
                for (int SIZE = 100; SIZE <= 5000; SIZE += 100)
                {
                    Random rnd = new Random();

                    int[] testArray = new int[SIZE];

                    for (int i = 0; i < testArray.Length; i++)
                    {
                        testArray[i] = rnd.Next();
                    }

                    Console.WriteLine("Test array with random elements...");
                    RunAllTests(testArray, "test_results\\random.csv");

                    for (int i = 0; i < testArray.Length; i++)
                    {
                        testArray[i] = i;
                    }

                    Console.WriteLine("Test array with All Ascending elements...");
                    RunAllTests(testArray, "test_results\\all_ascending.csv");

                    for (int i = 0; i < testArray.Length; i++)
                    {
                        testArray[i] = testArray.Length - i;
                    }

                    Console.WriteLine("Test array with All Descending elements...");
                    RunAllTests(testArray, "test_results\\all_descending.csv");

                    for (int i = 0; i < testArray.Length; i++)
                    {
                        testArray[i] = 0;
                    }

                    Console.WriteLine("Test array with All Same elements...");
                    RunAllTests(testArray, "test_results\\all_same.csv");
                }
            }
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }

        /// <summary>
        /// Runs all 4 test types with a given integer array.
        /// This is done 10 times, and the results of those 10 tests is averaged to obtain the final result.
        /// Results are then recorded in a csv-formatted file at the given filepath.
        /// Test 1: BruteForceMedian, Basic Operations,
        /// Test 2: BruteForceMedian, Execution TIme,
        /// Test 3: Median, Basic Operations,
        /// Test 4: Median, Execution Time
        /// </summary>
        /// <param name="testArray">The integer array to use in the tests.</param>
        /// <param name="filePath">The filepath to save the csv-formatted file containing the test results.</param>
        static void RunAllTests(int[] testArray, string filePath)
        {
            const int ATTEMPTS = 100;
            int median = 0;
            int[] bruteForceMedian_basicOps = new int[ATTEMPTS];
            long[] bruteForceMedian_execTime = new long[ATTEMPTS];
            int[] median_basicOps = new int[ATTEMPTS];
            long[] median_execTime = new long[ATTEMPTS];

            for (int i = 0; i < ATTEMPTS; i++)
            {
                Console.WriteLine("TEST ARRAY WITH SIZE OF: " + testArray.Length);
                Console.WriteLine("ATTEMPT " + i + " OF " + ATTEMPTS);
                Console.WriteLine();

                Console.WriteLine("TEST 1: BruteForceMedian -> Basic Operations");
                TestBruteForceMedianBasicOps(testArray, out median, out bruteForceMedian_basicOps[i]);
                Console.WriteLine("Median: " + median);
                Console.WriteLine("Basic Operations: " + bruteForceMedian_basicOps[i]);
                Console.WriteLine();

                Console.WriteLine("TEST 2: BruteForceMedian -> Execution Time");
                TestBruteForceMedianExecTime(testArray, out median, out bruteForceMedian_execTime[i]);
                Console.WriteLine("Median: " + median);
                Console.WriteLine("Execution Time: " + bruteForceMedian_execTime[i] + " ticks");
                Console.WriteLine();

                Console.WriteLine("TEST 3: Median -> Basic Operations");
                TestMedianBasicOps(testArray, out median, out median_basicOps[i]);
                Console.WriteLine("Median: " + median);
                Console.WriteLine("Basic Operations: " + median_basicOps[i]);
                Console.WriteLine();

                Console.WriteLine("TEST 4: BruteForceMedian -> Execution Time");
                TestMedianExecTime(testArray, out median, out median_execTime[i]);
                Console.WriteLine("Median: " + median);
                Console.WriteLine("Execution Time: " + median_execTime[i] + " ticks");
                Console.WriteLine();
            }

            Console.WriteLine("Averaging results from all 10 attemps...");
            double avg_bruteForceMedian_basicOps = 0;
            double avg_bruteForceMedian_execTime = 0;
            double avg_median_basicOps = 0;
            double avg_median_execTime = 0;

            for (int i = 0; i < ATTEMPTS; i++)
            {
                avg_bruteForceMedian_basicOps += Convert.ToDouble(bruteForceMedian_basicOps[i]);
                avg_bruteForceMedian_execTime += Convert.ToDouble(bruteForceMedian_execTime[i]);
                avg_median_basicOps += Convert.ToDouble(median_basicOps[i]);
                avg_median_execTime += Convert.ToDouble(median_execTime[i]);
            }

            avg_bruteForceMedian_basicOps /= (double)ATTEMPTS;
            avg_bruteForceMedian_execTime /= (double)ATTEMPTS;
            avg_median_basicOps /= (double)ATTEMPTS;
            avg_median_execTime /= (double)ATTEMPTS;

            Console.WriteLine("Avg. BruteForceMedian Basic Operations: " + avg_bruteForceMedian_basicOps);
            Console.WriteLine("Avg. BruteForceMedian Execution Time: " + avg_bruteForceMedian_execTime + " ticks");
            Console.WriteLine("Avg. Median Basic Operations: " + avg_median_basicOps);
            Console.WriteLine("Avg. Median Execution Time: " + avg_median_execTime + " ticks");
            Console.WriteLine();
            Console.WriteLine("Writing test result to " + filePath + "...");
            using (StreamWriter file = new StreamWriter(filePath, true))
            {
                file.WriteLine(testArray.Length + "," + avg_bruteForceMedian_basicOps + "," + avg_bruteForceMedian_execTime + "," + avg_median_basicOps + "," + avg_median_execTime);
            }
            Console.WriteLine("TEST FOR ARRAY WITH SIZE " + testArray.Length + " COMPLETE!" + Environment.NewLine);
        }

        /// <summary>
        /// Searches for and returns the median value in a given integer array
        /// Implementation of the Median function pseudocode as provided within assessment specification. 
        /// Used by the Median Algorithm.
        /// </summary>
        /// <param name="A">The given integer array to search.</param>
        /// <returns>The median value of elements within the array.</returns>
        static int Median(int[] A)
        {
            if (A.Length == 1)
                return A[0];
            else
                return Select(A, 0, (int)Math.Floor((double)(A.Length / 2.0)), A.Length - 1);
        }

        /// <summary>
        /// Searches between a given lower and upper bound within a given array, sorting it in the process,
        /// and once sorted, selects and returns the value of the element at the given midpoint locatin.
        /// Implementation of the Select function pseudocode as provided within assessment specification.
        /// Used by the Median algorithm.
        /// </summary>
        /// <param name="A">The integer array to search.</param>
        /// <param name="l">The lower bound location to search within.</param>
        /// <param name="m">the midpoint location within the array.</param>
        /// <param name="h">The upper bound location to search within.</param>
        /// <returns>The value of the element at the midpoint location (the median).</returns>
        static int Select(int[] A, int l, int m, int h)
        {
            int pos = Partition(A, l, h);

            if (pos == m)
                return A[pos];

            if (pos > m)
                return Select(A, l, m, pos - 1);

            if (pos < m)
                return Select(A, pos + 1, m, h);

            return -1;
        }

        /// <summary>
        /// Implementation of the Partition function pseudocode provided by assessment specification.
        /// Used by the Median algorithm.
        /// </summary>
        /// <param name="A">The integer array to sort and obtain a "pivot" location from.</param>
        /// <param name="l">The lower bound location to sort from.</param>
        /// <param name="h">The upper bound location to sort to.</param>
        /// <returns>The "pivot" location.</returns>
        static int Partition(int[] A, int l, int h)
        {
            int pivotval = A[l];
            int pivotloc = l;

            for (int j = l + 1; j <= h; j++)
            {
                if (A[j] < pivotval)
                {
                    pivotloc++;
                    A.Swap(pivotloc, j);
                }
            }
            A.Swap(l, pivotloc);
            return pivotloc;
        }

        /// <summary>
        /// Same as Median, but with added counter to count basic operations.
        /// </summary>
        /// <param name="A">The given integer array to search.</param>
        /// <returns>The median value of elements within the array.</returns>
        static int MedianWithCounter(int[] A)
        {
            basicOperations++;
            if (A.Length == 1)
                return A[0];
            else
                return SelectWithCounter(A, 0, (int)Math.Floor((double)(A.Length / 2.0)), A.Length - 1);
        }

        /// <summary>
        /// Same as Select, but with added counter to count basic operations.
        /// </summary>
        /// <param name="A">The integer array to search.</param>
        /// <param name="l">The lower bound location to search within.</param>
        /// <param name="m">the midpoint location within the array.</param>
        /// <param name="h">The upper bound location to search within.</param>
        /// <returns>The value of the element at the midpoint location (the median).</returns>
        static int SelectWithCounter(int[] A, int l, int m, int h)
        {
            int pos = PartitionWithCounter(A, l, h);

            basicOperations++;
            if (pos == m)
                return A[pos];

            basicOperations++;
            if (pos > m)
                return SelectWithCounter(A, l, m, pos - 1);

            basicOperations++;
            if (pos < m)
                return SelectWithCounter(A, pos + 1, m, h);

            return -1;
        }

        /// <summary>
        /// Same as Partition, but with added counter to count basic operations.
        /// </summary>
        /// <param name="A">The integer array to sort and obtain a "pivot" location from.</param>
        /// <param name="l">The lower bound location to sort from.</param>
        /// <param name="h">The upper bound location to sort to.</param>
        /// <returns>The "pivot" location.</returns>
        static int PartitionWithCounter(int[] A, int l, int h)
        {
            int pivotval = A[l];
            int pivotloc = l;

            for (int j = l + 1; j <= h; j++)
            {
                basicOperations++;
                if (A[j] < pivotval)
                {
                    pivotloc++;
                    A.Swap(pivotloc, j);
                }
            }
            A.Swap(l, pivotloc);
            return pivotloc;
        }

        /// <summary>
        /// Extension method to swap the values of two elements within a given integer array.
        /// </summary>
        /// <param name="A">The array within which to swap two elements.</param>
        /// <param name="firstPos">The location of the first element to swap.</param>
        /// <param name="secondPos">The location of the second element to swap.</param>
        static void Swap(this int[] A, int firstPos, int secondPos)
        {
            int temp = A[firstPos];
            A[firstPos] = A[secondPos];
            A[secondPos] = temp;
        }

        /// <summary>
        /// Searches for and returns the median value in a given integer array.
        /// Implmentation of the BruteForceMedian algorithm pseudocode provided by assessment specification.
        /// </summary>
        /// <param name="A">The integer array to search.</param>
        /// <returns>The median value of the elements within the array.</returns>
        static int BruteForceMedian(int[] A)
        {
            int k = (int)Math.Ceiling(A.Length / 2.0);
            for (int i = 0; i < A.Length; i++)
            {
                int numsmaller = 0;
                int numequal = 0;

                for (int j = 0; j < A.Length; j++)
                {
                    if (A[j] < A[i])
                    {
                        numsmaller++;
                    }
                    else
                    {
                        if (A[j] == A[i])
                        {
                            numequal++;
                        }
                    }
                }

                if (numsmaller < k && k <= (numsmaller + numequal))
                {
                    return A[i];
                }
            }
            return -1;
        }

        // efficiency is maybe 2n^2 - nln(n) - 2n + ln(n) ???

        /// <summary>
        /// Same as BruteForceMedian, but with an aded counter to cunt basic operations.
        /// </summary>
        /// <param name="A">The integer array to search.</param>
        /// <returns>The median value of the elements within the array.</returns>
        static int BruteForceMedianWithCounter(int[] A)
        {
            int k = (int)Math.Ceiling(A.Length / 2.0);
            for (int i = 0; i < A.Length; i++) 
            {
                int numsmaller = 0;
                int numequal = 0;

                for (int j = 0; j < A.Length; j++) 
                {
                    basicOperations++;
                    if (A[j] < A[i])   
                    {
                        numsmaller++;   
                    }
                    else
                    {
                        basicOperations++;
                        if (A[j] == A[i])   
                        {
                            numequal++;
                        }
                    }
                }

                basicOperations++;
                if (numsmaller < k && k <= (numsmaller + numequal)) 
                {   
                    return A[i];
                }
            }
            
            return -1;
        }

        /// <summary>
        /// Wrapper method to test Median's execution time with a given integer array.
        /// </summary>
        /// <param name="testArray">The integer array for Median to search.</param>
        /// <param name="median">Out var to store the returned median value.</param>
        /// <param name="execTime">Out var to store the returned execution time.</param>
        public static void TestMedianExecTime(int[] testArray, out int median, out long execTime)
        {
            Stopwatch timer = Stopwatch.StartNew();
            median = Median(testArray);
            timer.Stop();

            execTime = timer.ElapsedTicks;
        }

        /// <summary>
        /// Wrapper method to test BruteForceMedian's execution time with a given integer array.
        /// </summary>
        /// <param name="testArray">The integer array for BruteForceMedian to search.</param>
        /// <param name="median">Out var to store the returned median value.</param>
        /// <param name="execTime">Out var to store the returned execution time.</param>
        public static void TestBruteForceMedianExecTime(int[] testArray, out int median, out long execTime)
        {
            Stopwatch timer = Stopwatch.StartNew();
            median = BruteForceMedian(testArray);
            timer.Stop();

            execTime = timer.ElapsedTicks;
        }

        /// <summary>
        /// Wrapper method to count Median's basic operations with a give integer array.
        /// </summary>
        /// <param name="testArray">The integer array for Median to search.</param>
        /// <param name="median">Out var to store the returned median value.</param>
        /// <param name="basicOps">Out var to store the returned number of basic operations.</param>
        public static void TestMedianBasicOps(int[] testArray, out int median, out int basicOps)
        {
            basicOperations = 0;
            median = MedianWithCounter(testArray);
            basicOps = basicOperations;
        }

        /// <summary>
        /// Wrapper method to count BruteForceMedian's basic operations with a give integer array.
        /// </summary>
        /// <param name="testArray">The integer array for BruteForceMedian to search.</param>
        /// <param name="median">Out var to store the returned median value.</param>
        /// <param name="basicOps">Out var to store the returned number of basic operations.</param>
        public static void TestBruteForceMedianBasicOps(int[] testArray, out int median, out int basicOps)
        {
            basicOperations = 0;
            median = BruteForceMedianWithCounter(testArray);
            basicOps = basicOperations;
        }

        /// <summary>
        /// Tests the BruteForceMedian and Median implementations
        /// by running them to obtain the median values of known arrays
        /// and comparing the results with known median values for the aforementioned arrays.
        /// </summary>
        /// <returns>
        /// True if all median values returned were equal the corresponding known median values for each algorithm.
        /// False otherwise.
        /// </returns>
        static bool FunctionalityTest()
        {
            // Test Array with only one element
            int[] testOnlyOne = new int[] { 1 };
            const int testOnlyOne_Median = 1;

            // Test Arrays with Odd amount of elements
            // and known results
            int[] testOdd0 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            const int testOdd0_Median = 5;

            int[] testOdd1 = { 4, 5, 6, 7, 8, 9, 1, 2, 3, 0, 10 };
            const int testOdd1_Median = 5;

            int[] testOdd2 = { 4, 1, 10, 9, 7, 12, 8, 2, 15 };
            const int testOdd2_Median = 8;

            int[] testOdd3 = { 37, 94, 77, 63, 6, 49, 35 };
            const int testOdd3_Median = 49;

            int[] testOdd4 = { 37, 94, 77, 63, 6, 49, 35, 35, 35 };
            const int testOdd4_Median = 37;

            // Test arrays with Even number of elements
            // and known results for each algorithm
            int[] testEven0 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            const int testEven0_BruteForceMedian = 4;
            const int testEven0_Median = 5;

            int[] testEven1 = { 4, 5, 6, 7, 8, 9, 1, 2, 3, 0 };
            const int testEven1_BruteForceMedian = 4;
            const int testEven1_Median = 5;

            int[] testEven2 = { 4, 1, 10, 9, 7, 12, 8, 2, 15, 18 };
            const int testEven2_BruteForceMedian = 8;
            const int testEven2_Median = 9;

            int[] testEven3 = { 37, 94, 77, 63, 6, 49, 35, 22 };
            const int testEven3_BruteForceMedian = 37;
            const int testEven3_Median = 49;

            int[] testEven4 = { 37, 94, 77, 63, 6, 49, 35, 22, 37, 37 };
            const int testEven4_BruteForceMedian = 37;
            const int testEven4_Median = 37;

            // Only if all 10 above tests return correct results does the functional test return true
            return (
                BruteForceMedian(testOnlyOne) == testOnlyOne_Median &&
                Median(testOnlyOne) == testOnlyOne_Median &&
                BruteForceMedian(testOdd0) == testOdd0_Median &&
                Median(testOdd0) == testOdd0_Median &&
                BruteForceMedian(testOdd1) == testOdd1_Median &&
                Median(testOdd1) == testOdd1_Median &&
                BruteForceMedian(testOdd2) == testOdd2_Median &&
                Median(testOdd2) == testOdd2_Median &&
                BruteForceMedian(testOdd3) == testOdd3_Median &&
                Median(testOdd3) == testOdd3_Median &&
                BruteForceMedian(testOdd4) == testOdd4_Median &&
                Median(testOdd4) == testOdd4_Median &&

                BruteForceMedian(testEven0) == testEven0_BruteForceMedian &&
                Median(testEven0) == testEven0_Median &&
                BruteForceMedian(testEven1) == testEven1_BruteForceMedian &&
                Median(testEven1) == testEven1_Median &&
                BruteForceMedian(testEven2) == testEven2_BruteForceMedian &&
                Median(testEven2) == testEven2_Median &&
                BruteForceMedian(testEven3) == testEven3_BruteForceMedian &&
                Median(testEven3) == testEven3_Median &&
                BruteForceMedian(testEven4) == testEven4_BruteForceMedian &&
                Median(testEven4) == testEven4_Median
                );

        }
    }
}
